﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using Newtonsoft.Json;

namespace ExemploWorkbook.Formularios
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Properties.Settings.Default.URLCadastro);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            WebRequest webRequest = WebRequest.Create(Properties.Settings.Default.URLMetodos);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            // Monta a string para ser enviada via post para o servidor
            string Dados = String.Format("Acao=ValidarSerial&Email={0}&Serial={1}", txtEmail.Text, txtSerial.Text.ToUpper());

            byte[] dataStream = Encoding.UTF8.GetBytes(Dados);
            webRequest.ContentLength = dataStream.Length;
            System.IO.Stream newStream = webRequest.GetRequestStream();
            newStream.Write(dataStream, 0, dataStream.Length);  // Envia os dados para o servidor
            newStream.Close();

            WebResponse response = webRequest.GetResponse();
            System.IO.StreamReader streamReader = new System.IO.StreamReader(response.GetResponseStream());
            String responseData = streamReader.ReadToEnd(); // Retorna a resposta do servidor

            // Aqui transforma o JSON em um dicionário.
            // Há outros exemplos, veja em: http://james.newtonking.com/json/help/index.html?topic=html/Samples.htm
            Dictionary<string, string> htmlAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseData);

            if (htmlAttributes["erro"] == "0")
            {
                MessageBox.Show("O sistema foi ativado com sucesso!." + Environment.NewLine + htmlAttributes["msg"], "Ativação do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Properties.Settings.Default.RegistroNome = htmlAttributes["RegistroNome"];
                Properties.Settings.Default.RegistroEmail = htmlAttributes["RegistroEmail"];
                Properties.Settings.Default.RegistroSerial = htmlAttributes["RegistroSerial"];
                Properties.Settings.Default.Ativado = true;
                Properties.Settings.Default.Save();
                this.Close();
            
            }
            else
            {
                MessageBox.Show("O sistema não foi ativado." + Environment.NewLine + htmlAttributes["msg"], "Ativação do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
    }
}
