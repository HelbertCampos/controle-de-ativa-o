<?php
session_start();

// Percorre todos os POST criando uma variavel com o mesmo nome do POST
foreach($_POST as $nome_campo => $valor){ 
   $comando = "\$" . $nome_campo . "='" . $valor . "';"; 
   eval($comando); 
};

// Verifica se há ação a ser executada
if (!isset($Email) || $Email == '' || !isset($SenhaAcesso)) {
	$Resposta = array('erro'=>'1', 'msg'=>'Ação não definida.');
	echo json_encode($Resposta);
	exit;
};

//faz inclusão apenas das classes necessárias
require_once('ofapp.config.php');
require_once('ofapp.util.php');
$Util = new Util();

$UsuarioInfo = $Util->UsuarioInfo($Email);

if (isset($UsuarioInfo->IDUsuario)) {
	
	// Criptografa a senha digitada para comparar com a senha no banco de dados
	$HashSenhaDigitada = $Util->CriaHash($SenhaAcesso);
	
	if ($UsuarioInfo->SenhaAcesso == $HashSenhaDigitada) {
	
		if ($UsuarioInfo->_Ativo == '1') {
			
/*
			// Se usuário estiver ativo, incia a sessão do usuário	
            //$_SESSION['_IDCliente'] = $UsuarioInfo->_IDCliente;
            $_SESSION['IDUsuario'] = $UsuarioInfo->IDUsuario;
            $_SESSION['_IDHash'] = $UsuarioInfo->_IDHash;
            $_SESSION['Avatar'] = $UsuarioInfo->Avatar;
            $_SESSION['NomePrimeiro'] = $UsuarioInfo->NomePrimeiro;
            $_SESSION['NomeUltimo'] = $UsuarioInfo->NomeUltimo;
            $_SESSION['Email'] = $UsuarioInfo->Email;
            $_SESSION['_NivelUsuario'] = $UsuarioInfo->_NivelUsuario;
            $_SESSION['Logado'] = true;
            
            if ($UsuarioInfo->_NivelUsuario == '0') {
	            $_SESSION['_IDCliente'] = $UsuarioInfo->_IDCliente;
            }
*/
            

			// Reseta o contator de falha de login e grava a data e hora do login no banco de dados
			$Util->GravaAcessoUsuario($UsuarioInfo->IDUsuario);
			
			$resposta = array('erro'=>'0', 'msg'=>'Usuário autenticado com sucesso!', 'Serial'=>$UsuarioInfo->Serial);
				
		} else {
			// Se usuário não estiver ativo, mostra mensagem	
			$resposta = array('erro'=>'1', 'msg'=>'<strong>Conta inativa!</strong> Entre em contato com o administrador para ativar o seu acesso.');
		};	
	
	} else {
		
		if (($UsuarioInfo->_TentativasLogin >= 3) && ($UsuarioInfo->_UltimaTentativa > (time() - 30))) {
			// Se há mais de 3 tentativas de acesso em menos de 30 segundos, manda usuário esperar
			$resposta = array('erro'=>'1', 'msg'=>'<strong>Senha errada!</strong> Você informou a senha errada 3 ou mais vezes. Por favor, espere 30 segundos para tentar novamente.');			
		} else {
			// Se entrou com a senha errada grava mais uma tentativa no banco de dados
			$Util->GravaTentativaAcesso($UsuarioInfo->IDUsuario);
			$resposta = array('erro'=>'1', 'msg'=>'<strong>Senha errada!</strong> '.$UsuarioInfo->NomePrimeiro.' digite a sua senha corretamente.');			
		};
		
	};	
	
} else {
	$resposta = array('erro'=>'1', 'msg'=>'<strong>Email não encontrado!</strong> Confira seu email e tente novamente.');
};


echo json_encode($resposta);

?>