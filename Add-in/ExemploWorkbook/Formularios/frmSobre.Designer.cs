﻿namespace ExemploWorkbook.Formularios
{
    partial class frmSobre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSobre));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblVersao = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.linkSiteOffice = new System.Windows.Forms.LinkLabel();
            this.btFechar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Ativar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(12, 83);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(576, 46);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Add-in de Exemplo da OfficeApp";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVersao
            // 
            this.lblVersao.BackColor = System.Drawing.Color.Transparent;
            this.lblVersao.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersao.Location = new System.Drawing.Point(12, 132);
            this.lblVersao.Name = "lblVersao";
            this.lblVersao.Size = new System.Drawing.Size(576, 21);
            this.lblVersao.TabIndex = 2;
            this.lblVersao.Text = "v1.0.0 | 2015";
            this.lblVersao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblVersao.Visible = false;
            // 
            // lblCopyright
            // 
            this.lblCopyright.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyright.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.Location = new System.Drawing.Point(294, 256);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(294, 21);
            this.lblCopyright.TabIndex = 3;
            this.lblCopyright.Text = "| Copyright © OfficeApp 2015";
            this.lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // linkSiteOffice
            // 
            this.linkSiteOffice.BackColor = System.Drawing.Color.Transparent;
            this.linkSiteOffice.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.linkSiteOffice.ForeColor = System.Drawing.Color.White;
            this.linkSiteOffice.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkSiteOffice.LinkColor = System.Drawing.Color.White;
            this.linkSiteOffice.Location = new System.Drawing.Point(32, 258);
            this.linkSiteOffice.Name = "linkSiteOffice";
            this.linkSiteOffice.Size = new System.Drawing.Size(264, 17);
            this.linkSiteOffice.TabIndex = 4;
            this.linkSiteOffice.TabStop = true;
            this.linkSiteOffice.Text = "www.officeapp.com.br";
            this.linkSiteOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkSiteOffice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSiteOffice_LinkClicked);
            // 
            // btFechar
            // 
            this.btFechar.BackColor = System.Drawing.Color.White;
            this.btFechar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btFechar.ForeColor = System.Drawing.Color.DimGray;
            this.btFechar.Location = new System.Drawing.Point(492, 28);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 25);
            this.btFechar.TabIndex = 5;
            this.btFechar.Text = "Fechar";
            this.btFechar.UseVisualStyleBackColor = false;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.lblNome);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(129, 145);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(339, 99);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registrado para:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(59, 61);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(169, 17);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "helbert@officeapp.com.br";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(59, 32);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(107, 17);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Helbert Campos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Email:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // btn_Ativar
            // 
            this.btn_Ativar.BackColor = System.Drawing.Color.White;
            this.btn_Ativar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_Ativar.ForeColor = System.Drawing.Color.DimGray;
            this.btn_Ativar.Location = new System.Drawing.Point(411, 28);
            this.btn_Ativar.Name = "btn_Ativar";
            this.btn_Ativar.Size = new System.Drawing.Size(75, 25);
            this.btn_Ativar.TabIndex = 5;
            this.btn_Ativar.Text = "Ativar ...";
            this.btn_Ativar.UseVisualStyleBackColor = false;
            this.btn_Ativar.Click += new System.EventHandler(this.btn_Ativar_Click);
            // 
            // frmSobre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(600, 359);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Ativar);
            this.Controls.Add(this.btFechar);
            this.Controls.Add(this.linkSiteOffice);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lblVersao);
            this.Controls.Add(this.lblTitulo);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSobre";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sobre";
            this.Load += new System.EventHandler(this.frmSobre_Load);
            this.Click += new System.EventHandler(this.frmSobre_Click);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblVersao;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.LinkLabel linkSiteOffice;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Ativar;
    }
}