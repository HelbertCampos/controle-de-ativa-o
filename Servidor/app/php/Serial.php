<?php	/*! Sistema de Ativação | (c) 2014 OfficeApp | www.officeapp.com.br !*/

// Percorre todos os POST criando uma variavel com o mesmo nome do POST
foreach($_POST as $nome_campo => $valor){ 
   $comando = "\$" . $nome_campo . "='" . $valor . "';"; 
   eval($comando); 
};

// Verifica se há ação a ser executada
if (!isset($Acao) || $Acao == '') {
	$Resposta = array('erro'=>'1', 'msg'=>'Ação não definida.');
	echo json_encode($Resposta);
	exit;
};

// Faz inclusão das classes necessárias
require_once('ofapp.config.php');
require_once('ofapp.util.php');
$Util = new Util();

try {		
	
    switch ($Acao) {
	    case 'GerarSerial':

//			if ($Email == "helbertcampos@gmail.com") {
//                
//                if ($Serial == "0000-0000-0000-0000") {
//                    $Resposta = array('erro'=>'0', 'msg'=>'Email validado com o serial '.$Serial.'.', 'RegistroNome'=>'Helbert Campos', 'RegistroEmail'=>'helbertcampos@gmail.com', 'RegistroSerial'=>'AAAA-0000-AAAA-0000');
//                } else {
//                    $Resposta = array('erro'=>'1', 'msg'=>'Email foi validado, porém o serial '.$Serial.' não corresponde com o cadastro.');
//                }
//                
//            } else {
//                $Resposta = array('erro'=>'1', 'msg'=>'Email '.$Email.' não foi encontrado.');
//            }
        
            $flag=0; // Chave para continuar ou sair do loop
            
            //do {
                
                $cr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $max = strlen($cr)-1;
                $gera = null;

                for($i=0; $i < 16; $i++) {
                    $gera .= $cr{mt_rand(0, $max)};
                }
                
                $gera = str_split($gera, 4);
                $Serial = "$gera[0]-$gera[1]-$gera[2]-$gera[3]";
                
                // Aqui faz consulta no banco de dados para verificar se já há um serial cadastrado

                //$cont=0;    // Se achar um serial já cadastrado $cont deve ser igual a 1 para continuar o loop

                //if ($cont == 0) {
                    
                    // Chama query para inserir o Serial no banco de dados
					$ResultUPDATE = $Util->GravaSerial($IDUsuario, $Serial);
					$Resposta = $ResultUPDATE;

                    //$Resposta = array('erro'=>'0', 'msg'=>'Serial gerado com sucesso.', 'Serial'=>$Serial);

                    //$flag=1;
                    
                //}
                
            //} while($flag==1);

			break;
	    case 'ValidarSerial':
            
            if ($Email !== '' || $Serial !== '') {
                
                $UsuarioInfo = $Util->UsuarioInfo($Email);
                
                if (isset($UsuarioInfo->IDUsuario)) {
                    if ($UsuarioInfo->Serial == $Serial) {
                        $Resposta = array('erro'=>'0', 'msg'=>'Sistema ativado com sucesso!',
                                          'RegistroNome'=>$UsuarioInfo->NomePrimeiro.' '.$UsuarioInfo->NomeUltimo,
                                          'RegistroEmail'=>$UsuarioInfo->Email,
                                          'RegistroSerial'=>$UsuarioInfo->Serial);
                    } else {
                        $Resposta = array('erro'=>'1', 'msg'=>'Serial informado não está correto! Confira seu serial e tente novamente.');
                    };
                } else {
                    $Resposta = array('erro'=>'1', 'msg'=>'Email não encontrado! Confira seu email e tente novamente.');
                };
                
            } else {
                $Resposta = array('erro'=>'1', 'msg'=>'Email e/ou Serial não informado.');
            };

			break;
		default:
			$Resposta = array('erro'=>'1', 'msg'=>'Ação não reconhecida.');
	        break;
	};
    
} catch (Exception $e) {
	$Resposta = array('erro'=>'1', 'msg'=>'Erro de Classe! '.$e->getMessage());
};

//Saida:
echo json_encode($Resposta);

?>