<?php	/*! Sistema de Ativação | (c) 2014 OfficeApp | www.officeapp.com.br !*/
//session_start();

// Percorre todos os POST criando uma variavel com o mesmo nome do POST
foreach($_POST as $nome_campo => $valor){ 
   $comando = "\$" . $nome_campo . "='" . $valor . "';"; 
   eval($comando); 
};

// Verifica se há ação a ser executada
if (!isset($Acao) || $Acao == '') {
	$Resposta = array('erro'=>'1', 'msg'=>'Ação não definida.');
	echo json_encode($Resposta);
	exit;
};

// Faz inclusão das classes necessárias
require_once('ofapp.config.php');
require_once('ofapp.util.php');
$Util = new Util();

try {		
	switch ($Acao) {
	    case 'Info':					// ### retorna informações de um usuário

			$ResultSELECT = $Util->UsuarioInfoID($_IDUsuario);
			$Resposta = $ResultSELECT;

	        break;
	    case 'inserirNovoUsuario':		// ### Faz INSERT no banco de dados

			$Avatar = (($Avatar == "")?'001':trim($Avatar));
			$TelComercial = (($TelComercial == "")?NULL:trim($TelComercial));
			$TelCelular = (($TelCelular == "")?NULL:trim($TelCelular));
			$_IDCliente = (($_IDCliente == "")?NULL:trim($_IDCliente));

			$ResultINSERT = $Util->InserirUsuario($Avatar, $NomePrimeiro, $NomeUltimo, $Email, $SenhaAcesso, $TelComercial, $TelCelular, $_NivelUsuario, $_IDCliente);
			$Resposta = $ResultINSERT;
			
	        break;
	    case 'Ativar':					// ### Ativa usuário

			$ResultUPDATE = $Util->AtivarUsuario($IDUsuario);
			$Resposta = $ResultUPDATE;

			break;
	    case 'desativarUsuario':		// ### Desativa conta de usuário

			$ResultUPDATE = $Util->DesativarUsuario($IDUsuario);
			$Resposta = $ResultUPDATE;

			break;
	    case 'emailBoasVindasComSenha':		// ### Envia email com informações de login

		    $Dados[]=array("NomeCompleto", $NomeCompleto);
		    $Dados[]=array("Email", $Email);
		    $Dados[]=array("Senha", $SenhaAcesso);
		    $Dados[]=array("LinkPortal", URL_PORTAL);
		    $Template = 'bemvindo.comsenha.html';

			$ConteudoHTML = $Util->FormataEmail($Dados, $Template);
			if ($ConteudoHTML['erro'] == '0') {

				$ConteudoEmail = $ConteudoHTML['HTML'];
				$Assunto = 'Bem-vindo ao Portal Interativa';
			    $Categ = array('Usuário', 'Bem-vindo');
			    $Destinatarios[] = array('name'=>$NomeCompleto, 'email'=>$Email);
	
			    $EnviouEmail = $Util->EnviaEmailMandrill($ConteudoEmail, $Assunto, $Categ, $Destinatarios);
			    $Resposta = $EnviouEmail;
				
			} else {
		    	$Resposta = $ConteudoHTML;				
			};
			
	        break;
		default:
			$Resposta = array('erro'=>'1', 'msg'=>'Ação não reconhecida.');
	        break;
	};
} catch (Exception $e) {
	$Resposta = array('erro'=>'1', 'msg'=>'<strong>Erro de Classe!</strong> '.$e->getMessage());
};

//Saida:
echo json_encode($Resposta);

?>