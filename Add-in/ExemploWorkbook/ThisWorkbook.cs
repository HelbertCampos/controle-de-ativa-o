﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

// Como publicar a planilha (via internet, mostra como copiar a planilha para dentro do computador do usuario)
// http://msdn.microsoft.com/pt-br/library/bb772100.aspx

namespace ExemploWorkbook
{
    public partial class ThisWorkbook
    {
        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            // Se sistema não estiver ativado, mostra a mensagem
            if (!Properties.Settings.Default.Ativado)
            {
                ExemploWorkbook.Formularios.MsgRegistro frm = new ExemploWorkbook.Formularios.MsgRegistro();
                frm.ShowDialog();  
            }

            // Após axibir as mensagens, verifica o status do sistema e ativo ou não os controles
            if (Properties.Settings.Default.Ativado)
            {
                Globals.Ribbons.RibbonPrincipal.SistemaAtivo();
            }
            else
            {
                Globals.Ribbons.RibbonPrincipal.SistemaInativo();
            }

        }

        private void ThisWorkbook_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisWorkbook_Startup);
            this.Shutdown += new System.EventHandler(ThisWorkbook_Shutdown);
        }

        #endregion

    }
}
