﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExemploWorkbook.Formularios
{
    public partial class MsgRegistro : Form
    {
        public MsgRegistro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExemploWorkbook.Formularios.Login frm = new ExemploWorkbook.Formularios.Login();
            frm.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Properties.Settings.Default.URLCadastro);
            this.Close();
        }
    }
}
