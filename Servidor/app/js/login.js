/*! Sistema de Ativação | (c) 2014 OfficeApp | www.officeapp.com.br !*/

// Executa só depois que o DOM está acessível
$(document).ready(function () {

	var usuarios = new util.Usuarios();
	var msg = new util.Alertas("#MsgAlerta");

	// Valida formulário de login
	$("#LoginForm").validate({
		rules: {
		    Email: {
		        required: true,
		        email: true,
		        maxlength: 64,
		    },
		    SenhaAcesso: {
		        required: true,
		        minlength: 6,
		    }
		},
		messages: {
		    Email: {
		        required: "Informe seu email para entrar no portal.",
		        maxlength: jQuery.validator.format("O email não pode ter mais que {0} caracteres."),
		        email: "Informe um email válido.",
		    },
		    SenhaAcesso: {
		        required: "A senha não pode ser em branco.",
		        minlength: jQuery.validator.format("A senha deve ter no mínimo {0} caracteres."),
		    }
		},
		submitHandler: function(form) {
		
			//msg.carregando(' Validando usuário ...');
		
			var loginInfo = usuarios.validar($(form).serialize());
			
			if (loginInfo.erro == '0') {
				//location.href='index.html';
				
                $('#EmailSerial').val($('#Email').val());
                $('#Serial').val(loginInfo.Serial);
                $('#LoginForm').slideToggle("fast","linear");
                $('#SerialGerado').slideToggle("fast","linear");
				
			} else {
				msg.erro(loginInfo.msg);
			};
			
		}
	});

	// Validação do campo de email para renovar a senha
	$("#RenovarSenha").validate({
		rules: {
		    Email: {
		        required: true,
		        email: true,
		        maxlength: 64,
		    }
		},
		messages: {
		    Email: {
		        required: "Informe seu email para renovar sua senha de acesso.",
		        maxlength: jQuery.validator.format("O email não pode ter mais que {0} caracteres."),
		        email: "Informe um email válido.",
		    }
		},
		submitHandler: function(form) {
			if (usuarios.validarEmail($(form).serialize())) {		// Valida se o email digitado existe na base de dados
				usuarios.emailResetSenha($(form).serialize());		// Se o email exite, envia email para renovação da senha
			};
		},
		invalidHandler: function(event, validator) {
			$('.modal-footer .alert').fadeOut();
		},	
	});

	$('#ModalRenovarSenha .ModalOk').click(function() {
		$('#RenovarSenha').submit();
	});
});

// Executa só depois que tudo carregou
$(window).load(function() {

});