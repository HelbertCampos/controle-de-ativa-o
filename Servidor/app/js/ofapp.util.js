/*! Sistema de Ativação | (c) 2015 OfficeApp | www.officeapp.com.br !*/

// Configurações básicas para as requisições ajax
$.ajaxSetup({
    async: false,		// Executa outras funções apenas quando o ajax retornar uma resposta
    type: 'POST',
	dataType: 'json',
	cache: false,	
});

// Objeto comum para toda a aplicação
var util = {};

// Classe para controle de acesso de usuários ao portal
util.Usuarios = function() {
	var self = this;

	//var msg = new util.Alertas('#MsgAlerta');		// Inicializa a classe de alertas

	// Valida email e senha de acesso do usuário
	self.validar = function(argDados) {
		var resposta = {erro:'1', msg:'Resposta não definida.'};
		$.ajax({
			url: 'app/php/LoginPortal.php',
			data: argDados,
			error: function(request){
				console.log(request);
				resposta = {erro:'1', msg:'Ocorreu algum erro na classe util! Código ' + request.status + ' | ' +request.statusText};
			},
			success: function(r){
				resposta = r;
			},
		});
		return resposta;
	};
	
	// Retorna todas as informações de um usuário
	self.info = function(argID) {
	    var resposta = {erro:'1', msg:'Resposta não definida.'};
	    var dadosPOST = {
	        Acao: 'Info',
	        _IDUsuario: argID
	    };
	    $.ajax({
	        url: 'app/php/Usuarios.php',
	        data: dadosPOST,
	        error: function(request){
	            resposta = {erro:'1', msg:'Ocorreu algum erro ao resgatar as informações. Código ' + request.status + ' | ' +request.statusText};
	        },
	        success: function(r){
	            resposta = r;
	        },
	    });
	    return resposta;
	};

	// Cadastra um novo usuário no banco de dados
	self.inserirNovoUsuario = function(argDados) {
		argDados.Acao = 'inserirNovoUsuario';		// Adiciona a chave de ação junto aos dados
		var resposta = {erro:'1', msg:'Resposta não definida.'};
		$.ajax({
			url: 'app/php/Usuarios.php',
			data: argDados,
			//ajaxSend: msg.carregando('Aguarde, cadastrando novo usuário ...'),
			error: function(request){
				console.log(request);
				resposta = {erro:'1', msg:'Ocorreu algum erro na classe util! Código ' + request.status + ' | ' +request.statusText};
			},
			success: function(r){
				resposta = r;
			},
		});
		return resposta;
	};
	
};

// Classe para exibição de alertas para o usuário
util.Alertas = function(argElemento) {
	var self = this;

	// Elemento onde serão exibidas as mensagens
	var el = $(argElemento);

	this.opcoes = {
		iconCarregando: '<i class="fa fa-refresh fa-spin"></i> ',
		msgCarregando:'Processo em andamento ...',
		classCarregando:'alert-info',
		iconErro:'<i class="fa fa-exclamation-circle"></i> ',
		msgErro:'Erro na requisição ao servidor.',
		classErro: 'alert-danger',
		iconAtencao:'<i class="fa fa-exclamation-circle"></i> ',
		msgAtencao:'Algo requer atenção.',
		classAtencao: 'alert-warning',
		iconSucesso:'<i class="fa fa-check-circle"></i> ',
		msgSucesso:'Informações salvas com sucesso!',
		classSucesso: 'alert-success'
	};

	self.esconder = function() {
		el.fadeOut();
	};

	self.limpar = function() {
		el.html('');
		el.removeClass(self.opcoes.classErro).removeClass(self.opcoes.classCarregando).removeClass(self.opcoes.classSucesso).removeClass(self.opcoes.classAtencao);
	};

	self.carregando = function(argDesc) {
		self.limpar();
		if (argDesc !== undefined) {
			el.html(self.opcoes.iconCarregando + argDesc).addClass(self.opcoes.classCarregando);			
		} else {
			el.html(self.opcoes.iconCarregando + self.opcoes.msgCarregando).addClass(self.opcoes.classCarregando);
		};
		el.fadeIn();
	};

	self.atencao = function(argDesc) {
		self.limpar();
		if (argDesc !== undefined) {
			el.html(self.opcoes.iconAtencao + argDesc).addClass(self.opcoes.classAtencao);			
		} else {
			el.html(self.opcoes.iconAtencao + self.opcoes.msgAtencao).addClass(self.opcoes.classAtencao);
		};
		el.fadeIn();
	};

	self.erro = function(argDesc) {
		self.limpar();
		if (argDesc !== undefined) {
			el.html(self.opcoes.iconErro + argDesc).addClass(self.opcoes.classErro);			
		} else {
			el.html(self.opcoes.iconErro + self.opcoes.msgErro).addClass(self.opcoes.classErro);
		};
		el.fadeIn();
	};

	self.sucesso = function(argDesc) {
		self.limpar();
		if (argDesc !== undefined) {
			el.html(self.opcoes.iconSucesso + argDesc).addClass(self.opcoes.classSucesso);			
		} else {
			el.html(self.opcoes.iconSucesso + self.opcoes.msgSucesso).addClass(self.opcoes.classSucesso);
		};
		el.fadeIn();
	};
};

// Classe para exibição de modal para o usuário
util.Modals = function(argElemento) {
	var self = this;

	// Elemento do modal onde serão exibidas as mensagens
	var el = $(argElemento);

	this.opcoes = {
		Titulo: 'Título do Modal',
		Mensagem:'Mensagem do Modal',
		OkTexto:'Ok',
		OkClasse:'btn-primary',		// btn-warning
		MostrarOk: true
	};

	self.mostrar = function() {
		el.find('#ModalTitulo').html(self.opcoes.Titulo);
		el.find('#ModalMensagem').html(self.opcoes.Mensagem);
		if (self.opcoes.MostrarOk) {
			el.find('#ModalOk').html(self.opcoes.OkTexto);
			el.find('#ModalOk').removeClass().addClass('btn ' + self.opcoes.OkClasse);
		} else {
			el.find('#ModalOk').hide();
		};
		el.modal('show');
	};

	self.fechar = function() {
		el.modal('hide');
	};

	self.BotaoOk = function() {
		return el.find('#ModalOk');
	};

	self.Titulo = function(argStr) {
		if (argStr == undefined) {
			return self.opcoes.Titulo;
		} else {
			self.opcoes.Titulo = argStr;
		};
	};

	self.Mensagem = function(argStr) {
		if (argStr == undefined) {
			return self.opcoes.Mensagem;			
		} else {
			self.opcoes.Mensagem = argStr;
		};
	};

	self.OkTexto = function(argStr) {
		if (argStr == undefined) {
			return self.opcoes.OkTexto;			
		} else {
			self.opcoes.OkTexto = argStr;
		};
	};

	self.OkClasse = function(argStr) {
		if (argStr == undefined) {
			return self.opcoes.OkClasse;			
		} else {
			self.opcoes.OkClasse = argStr;
		};
	};
	
	self.MostrarOk = function(argStr) {
		if (argStr == undefined) {
			return self.opcoes.MostrarOk;
		} else {
			self.opcoes.MostrarOk = argStr;
		};
	};
};

// Classe para controle dos seriais cadastrados
var Serial = function() {
	var self = this;

	this.opcoes = {
	};

	//var msg = new util.Alertas('#MsgAlerta');		// Inicializa a classe de alertas

	// Gera serial único para usuário
	self.GerarSerial = function(argID) {
		var resposta = {erro:'1', msg:'Resposta não definida.'};
		var argDados = {
			Acao: 'GerarSerial',
			IDUsuario: argID
		};
		$.ajax({
			url: 'app/php/Serial.php',
			data: argDados,
			error: function(request){
				resposta = {erro:'1', msg:'Ocorreu algum erro na classe util! Código ' + request.status + ' | ' +request.statusText};
                console.log(request);
			},
			success: function(r){
				resposta = r;
			},
		});
		return resposta;
	};

};