/*! Sistema de Ativação | (c) 2014 OfficeApp | www.officeapp.com.br */

// Executa só depois que o DOM está acessível
$(document).ready(function () {
    
    var usuarios = new util.Usuarios();
    var serial = new Serial();
    var msg = new util.Alertas("#MsgAlerta");
    
	// Valida formulário com informações do novo usuario
	$('#NovoRegistro').validate({
        rules: {
            NomePrimeiro: {
                required: true,
                minlength: 3,
                maxlength: 45,
            },
            NomeUltimo: {
                required: true,
                minlength: 3,
                maxlength: 45,
            },
		    Email: {
		        required: true,
		        email: true,
		        maxlength: 64,
		        //ValidaEmailCadastrado: true,
		    },
            Empresa: {
                required: true,
                minlength: 3,
                maxlength: 45,
            },
            Cargo: {
                required: true,
                minlength: 3,
                maxlength: 45,
            },
            SenhaAcesso: {
                required: true,
                minlength: 6,
            },
		    SenhaAcessoConfir: {
	            equalTo: "#SenhaAcesso",
			},
            Termo: {
                required: true,
            },
		},
        messages: {
		    NomePrimeiro: {
		        required: "O primeiro nome não pode ficar em branco.",
		        minlength: jQuery.validator.format("O nome deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o nome não pode ter mais que {0} caracteres."),
		    },
		    NomeUltimo: {
		        required: "O último nome não pode ficar em branco.",
		        minlength: jQuery.validator.format("O nome deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o nome não pode ter mais que {0} caracteres."),
		    },
		    Email: {
		        required: "O email não pode ficar em branco.",
		        maxlength: jQuery.validator.format("Por favor, o email não pode ter mais que {0} caracteres."),
		        email: "Informe um email válido.",
		    },
		    Empresa: {
		        required: "O nome da empresa não pode ficar em branco.",
		        minlength: jQuery.validator.format("O nome deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o nome não pode ter mais que {0} caracteres."),
		    },
		    Cargo: {
		        required: "O cargo não pode ficar em branco.",
		        minlength: jQuery.validator.format("O cargo deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o cargo não pode ter mais que {0} caracteres."),
		    },
		    SenhaAcesso: {
		        required: "A senha não pode estar em branco.",
		        minlength: jQuery.validator.format("A senha deve ter no mínimo {0} caracteres."),
		    },
		    SenhaAcessoConfir: {
		        equalTo: "Repita a senha corretamente.",
		    },
		    Termo: {
		        required: "Aceite os termos para gerar o serial de ativação.",
		    },
        },
        submitHandler: function(frm) {

			msg.carregando(' Cadastrando novo usuário ...');

            var argDados = {
                Avatar: '001',
                NomePrimeiro: $(frm).find('#NomePrimeiro').val(),
                NomeUltimo: $(frm).find('#NomeUltimo').val(),
                Email: $(frm).find('#Email').val(),
                SenhaAcesso: $(frm).find('#SenhaAcesso').val(),
                TelComercial: '',
                TelCelular: '',
                _NivelUsuario: '1',
                _IDCliente: '1'
            };

            var dadosUsuario = usuarios.inserirNovoUsuario(argDados);	// Chama ação de cadastro de novo usuário
            if (dadosUsuario.erro == '0') {
            	//msg.sucesso('Usuário cadastrado com sucesso!');

                
				msg.carregando(' Gerando novo serial ...');
	            
	            var serialGerado = serial.GerarSerial(dadosUsuario.IDUsuario);
	            
	            if (serialGerado.erro == '0') {
	                $('#EmailSerial').val($('#Email').val());
	                $('#Serial').val(serialGerado.Serial);
	                $('#NovoRegistro').slideToggle("fast","linear");
	                $('#SerialGerado').slideToggle("fast","linear");
	            } else {
	                msg.erro(serialGerado.msg);
	            }
                
            } else {
            	msg.erro(dadosUsuario.msg);
            };
        
        }
    });

});

// Executa só depois que tudo carregou
$(window).load(function() {

});