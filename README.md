# Sistema de Ativação
######Versão Atual 1.0.0

Sistema de ativação via serial com 16 dígitos.

## Demonstração
O Sistema em funcionamento pode ser visto aqui: [http://dev.officeapp.com.br/controle-ativacao](http://dev.officeapp.com.br/controle-ativacao)

## Plugins Utilizados
Abaixo estão listados os plugins utilizados para criação e funcionamento do sistema.

* [jQuery](http://jquery.com/) | v?.?.?