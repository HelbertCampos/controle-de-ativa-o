﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace ExemploWorkbook
{
    public partial class Plan1
    {
        private void Plan1_Startup(object sender, System.EventArgs e)
        {

            //this.Range["D14"].Value2 = "testes";
            //this.Names.Item("NomeUsuario").RefersToRange.Value2 = Environment.UserName;
            //this.Names.Item("Sistema").RefersToRange.Value2 = Environment.MachineName;
            //this.Names.Item("OS").RefersToRange.Value2 = Environment.OSVersion;
            //this.Names.Item("Procs").RefersToRange.Value2 = Environment.ProcessorCount;

        }

        private void Plan1_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(Plan1_Startup);
            this.Shutdown += new System.EventHandler(Plan1_Shutdown);
        }

        #endregion

    }
}
