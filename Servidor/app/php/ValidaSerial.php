<?php	/*! Sistema de Ativação | (c) 2014 OfficeApp | www.officeapp.com.br !*/

// Percorre todos os POST criando uma variavel com o mesmo nome do POST
foreach($_POST as $nome_campo => $valor){ 
   $comando = "\$" . $nome_campo . "='" . $valor . "';"; 
   eval($comando); 
};

// Verifica se há ação a ser executada
if (!isset($Acao) || $Acao == '') {
	$Resposta = array('erro'=>'1', 'msg'=>'Ação não definida.');
	echo json_encode($Resposta);
	exit;
};

try {		
	switch ($Acao) {
	    case 'Validar':

			if ($Email == "helbertcampos@gmail.com") {
                
                if ($Serial == "0000-0000-0000-0000") {
                    $Resposta = array('erro'=>'0', 'msg'=>'Email validado com o serial '.$Serial.'.', 'RegistroNome'=>'Helbert Campos', 'RegistroEmail'=>'helbertcampos@gmail.com', 'RegistroSerial'=>'AAAA-0000-AAAA-0000');
                } else {
                    $Resposta = array('erro'=>'1', 'msg'=>'Email foi validado, porém o serial '.$Serial.' não corresponde com o cadastro.');
                }
                
            } else {
                $Resposta = array('erro'=>'1', 'msg'=>'Email '.$Email.' não foi encontrado.');
            }

			break;
		default:
			$Resposta = array('erro'=>'1', 'msg'=>'Ação não reconhecida.');
	        break;
	};
} catch (Exception $e) {
	$Resposta = array('erro'=>'1', 'msg'=>'<strong>Erro de Classe!</strong> '.$e->getMessage());
};

//Saida:
echo json_encode($Resposta);

?>