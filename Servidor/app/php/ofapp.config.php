<?php
/*~ ofapp.config.php
.---------------------------------------------------------------------------.
|    Classe: Classe ofapp.config - Configurações do Portal	                |
|   Version: 1.0.0                                                          |
|      Site: http://www.officeapp.com.br/                                   |
| ------------------------------------------------------------------------- |
|   Cliente: Interativa Consultoria & Outsourcing			                |
| ------------------------------------------------------------------------- |
|   Author : Helbert Campos (OfficeApp) helbert@officeapp.com.br			|
| Copyright (c) 2014, Helbert Campos. Todos os direitos reservados.			|
| ------------------------------------------------------------------------- |
| Essa classe é distribuida juntamente com os produtos e serviços			|
| prestados pela OfficeApp.													|
| A distribuíção ou uso não autorizado não estão permitidos					|
'---------------------------------------------------------------------------'
*/

 
 /*
 * Versão atual do sistema	
*/
define("NOME_SISTEMA", "Sistema de Ativação");
define("NOME_EMPRESA", "OfficeApp");
define("VERSAO", "1.0.0");
define("DATA_VERSAO", "2015.01.20");

 /*
 * Definições do Banco de Dados MySQL	
*/

// ### BANCO DE PRODUÇÃO
//define("DB_HOST", "mysql02.site1382469184.hospedagemdesites.ws");
//define("DB_NAME", "site13824691841");
//define("DB_USER", "site13824691841");
//define("DB_PASS", "Inter@Consult");
//define("URL_PORTAL", "http://portal.interativaconsultoria.com.br/");

// ### BANCO DE HOMOLOGAÇÃO
//define("DB_HOST", "localhost");
//define("DB_NAME", "sistema_ativacao");
//define("DB_USER", "OfAppAdmin");
//define("DB_PASS", "OfficeApp@2014");
//define("URL_PORTAL", "http://dev.officeapp.com.br/controle-ativacao/Servidor/");

// ### BANCO DE DESENVOLVIMENTO
define("DB_HOST", "localhost");
define("DB_NAME", "sistema_ativacao");
define("DB_USER", "root");
define("DB_PASS", "");
define("URL_PORTAL", "http://localhost/godaddy/dev/controle-ativacao/Servidor/");

 /*
 * Definições de segurança
 * hash para ser integrado as senhas para depois disso possa ser criptografada	
*/
define("HASH_SENHA", "Construa seus próprios sonhos!");

 /*
 * Caminho de diretórios do sistema
*/
define("CAMINHO_MODELOS_EMAIL", "../../assets/mod/emails/");

 /*
 * Definições para envio de emails
*/
define('API_KEY', 'ZulO-9QCpA65_DCUMqbgDA');
define('EMAIL_FROM', 'officeapp@officeapp.com.br');
define('EMAIL_FROM_NAME', 'OfficeApp');

?>