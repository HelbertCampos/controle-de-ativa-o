﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;

namespace ExemploWorkbook
{
    public partial class RibbonPrincipal
    {
        private void RibbonPrincipal_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btn_AbrirForm_Click(object sender, RibbonControlEventArgs e)
        {


        }

        private void btn_Sobre_Click(object sender, RibbonControlEventArgs e)
        {
            ExemploWorkbook.Formularios.frmSobre frm = new ExemploWorkbook.Formularios.frmSobre();
            frm.ShowDialog();  
        }

        public void SistemaAtivo()
        {
            btn_AbrirForm.Enabled = true;
            btn_AbrirPainel.Enabled = true;
            lbl_Ativado.Label = "Sistema Ativo";
        }

        public void SistemaInativo()
        {
            btn_AbrirForm.Enabled = false;
            btn_AbrirPainel.Enabled = false;
            lbl_Ativado.Label = "Sistema Inativo";
        }


    }
}
