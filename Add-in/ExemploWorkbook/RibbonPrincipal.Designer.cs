﻿namespace ExemploWorkbook
{
    partial class RibbonPrincipal : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public RibbonPrincipal()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabOfApp = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btn_AbrirForm = this.Factory.CreateRibbonButton();
            this.btn_AbrirPainel = this.Factory.CreateRibbonToggleButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.btn_Sobre = this.Factory.CreateRibbonButton();
            this.lbl_Ativado = this.Factory.CreateRibbonLabel();
            this.tabOfApp.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            // 
            // tabOfApp
            // 
            this.tabOfApp.Groups.Add(this.group1);
            this.tabOfApp.Groups.Add(this.group2);
            this.tabOfApp.Label = "OFFICEAPP";
            this.tabOfApp.Name = "tabOfApp";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btn_AbrirForm);
            this.group1.Items.Add(this.btn_AbrirPainel);
            this.group1.Label = "Ferramentas";
            this.group1.Name = "group1";
            // 
            // btn_AbrirForm
            // 
            this.btn_AbrirForm.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_AbrirForm.Label = "Abrir Formulário";
            this.btn_AbrirForm.Name = "btn_AbrirForm";
            this.btn_AbrirForm.OfficeImageId = "FormControlInsertMenu";
            this.btn_AbrirForm.ShowImage = true;
            this.btn_AbrirForm.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_AbrirForm_Click);
            // 
            // btn_AbrirPainel
            // 
            this.btn_AbrirPainel.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_AbrirPainel.Label = "Exibir Painel";
            this.btn_AbrirPainel.Name = "btn_AbrirPainel";
            this.btn_AbrirPainel.OfficeImageId = "TableDataPane";
            this.btn_AbrirPainel.ShowImage = true;
            // 
            // group2
            // 
            this.group2.Items.Add(this.btn_Sobre);
            this.group2.Items.Add(this.lbl_Ativado);
            this.group2.Label = "OfficeApp";
            this.group2.Name = "group2";
            // 
            // btn_Sobre
            // 
            this.btn_Sobre.Label = "Sobre";
            this.btn_Sobre.Name = "btn_Sobre";
            this.btn_Sobre.OfficeImageId = "About";
            this.btn_Sobre.ShowImage = true;
            this.btn_Sobre.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_Sobre_Click);
            // 
            // lbl_Ativado
            // 
            this.lbl_Ativado.Label = "Produto Ativado";
            this.lbl_Ativado.Name = "lbl_Ativado";
            // 
            // RibbonPrincipal
            // 
            this.Name = "RibbonPrincipal";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tabOfApp);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.RibbonPrincipal_Load);
            this.tabOfApp.ResumeLayout(false);
            this.tabOfApp.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabOfApp;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_AbrirForm;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btn_AbrirPainel;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lbl_Ativado;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_Sobre;
    }

    partial class ThisRibbonCollection
    {
        internal RibbonPrincipal RibbonPrincipal
        {
            get { return this.GetRibbon<RibbonPrincipal>(); }
        }
    }
}
