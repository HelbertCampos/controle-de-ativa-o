﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExemploWorkbook.Formularios
{
    public partial class frmSobre : Form
    {
        public frmSobre()
        {
            InitializeComponent();
        }

        private void frmSobre_Load(object sender, EventArgs e)
        {
            lblVersao.Text = String.Format("v{0} | 2015", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            
            if (!Properties.Settings.Default.Ativado)
            {
                lblNome.Text = "<não registrado>";
                lblEmail.Text = "<não registrado>";
            }
            else
            {
                lblNome.Text = Properties.Settings.Default.RegistroNome;
                lblEmail.Text = Properties.Settings.Default.RegistroEmail;
            }

        
        }

        private void linkSiteOffice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void frmSobre_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Ativar_Click(object sender, EventArgs e)
        {
            ExemploWorkbook.Formularios.Login frm = new ExemploWorkbook.Formularios.Login();
            frm.ShowDialog();
            this.Close();
        }


    }
}
