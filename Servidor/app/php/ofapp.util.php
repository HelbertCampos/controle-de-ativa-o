<?php
/*~ ofapp.util.php
.---------------------------------------------------------------------------.
|    Classe: Classe ofapp.util - Controle de acesso aos dados               |
|   Version: 1.0.0                                                          |
|      Site: http://www.officeapp.com.br/                                   |
| ------------------------------------------------------------------------- |
|   Cliente: Interativa Consultoria & Outsourcing			                |
| ------------------------------------------------------------------------- |
|   Author : Helbert Campos (OfficeApp) helbert@officeapp.com.br			|
| Copyright (c) 2014, Helbert Campos. Todos os direitos reservados.			|
| ------------------------------------------------------------------------- |
| Essa classe é distribuida juntamente com os produtos e serviços			|
| prestados pela OfficeApp.													|
| A distribuíção ou uso não autorizado não estão permitidos					|
'---------------------------------------------------------------------------'
*/

/**
 *
 * CLASSE UTIL
 * @desc Funções comuns dentre todo o sistema
 * @author Helbert Campos | helbert@officeapp.com.br
 *
 */

class Util {

	// Cria a variável da conexão
	// que será usada em toda classe
    private $conexao = null;

	public function Util() {
		
	}


	/*	--------------------------------------------------------
	*	FUNÇÕES UTEIS PARA TODA A CLASSE
   */	

	// Faz conexão com o banco de dados
	// Retorna 'true' ou 'false'
    function ConexaoMySQL() {

        if ($this->conexao != null) {
            return true;
        } else {
            try {
                $this->conexao = new PDO('mysql:host='. DB_HOST .';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); //$this->conexao = new PDO('mysql:host='. DB_HOST .';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
                return true;
            } catch (PDOException $e) {
                return false;
            };
        };
        // default return
        return false;
    }

	// Cria e grava o Hash do $ID na tabela $NomeTabela
	// $NomeTabela: nome da tabela onde se deseja gravar o hash
	// $ID: id do registro que se deseja gravar o hash
	function GravaHash($NomeTabela, $CampoID, $ID, &$HashGravado) {
	
		$HashID = $this->CriaHash($ID);
		
		if ($this->ConexaoMySQL()) {
			try {
				$qry = $this->conexao->prepare('UPDATE '.$NomeTabela.' SET _IDHash = :IDHash WHERE '.$CampoID.' = :ID');
			    $qry->bindValue(':IDHash', 	$HashID,	PDO::PARAM_STR);
			    $qry->bindValue(':ID', 		$ID,		PDO::PARAM_INT);
				$qry->execute();
				if ($qry->rowCount() == 1) {
					$HashGravado = $HashID; //Retorna o Hash que criou para alguma ação de callback
					return true;
				} else {
					return false;
				};
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;	
		};	
	}

	/*
	*	FUNÇÕES UTEIS PARA TODA A CLASSE
   *    -------------------------------------------------------- */	


	/*	--------------------------------------------------------
	*	MÓDULO DE 'USUÁRIOS'
   */	

	// Retorna informações do usuário se ele existir. Se não existir, retorna FALSE
	// $Email: email do Usuário que deseja retornar as informações
	function UsuarioInfo($Email) {

		$VIEW = "
			SELECT 
			    tbl_Usuarios . *,
			    concat_ws(' ',
			            tbl_Usuarios.NomePrimeiro,
			            tbl_Usuarios.NomeUltimo) AS NomeCompleto
			FROM
			    tbl_Usuarios			
		";
	
		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare($VIEW.' WHERE tbl_Usuarios.Email = :Email LIMIT 1');
			    $qry->bindValue(':Email', $Email, PDO::PARAM_STR);
			    $qry->execute();
			    return $qry->fetchObject();
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;
		};

	}

	// Retorna informações do usuário se ele existir. Se não existir, retorna FALSE
	// $ID: id do Usuário que deseja retornar as informações
	function UsuarioInfoID($ID) {

		$VIEW = "
			SELECT 
			    tbl_Usuarios . *,
			    concat_ws(' ',
			            tbl_Usuarios.NomePrimeiro,
			            tbl_Usuarios.NomeUltimo) AS NomeCompleto
			FROM
			    tbl_Usuarios			
		";
	
		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare($VIEW.' WHERE tbl_Usuarios.IDUsuario = :IDUsuario LIMIT 1');
			    $qry->bindValue(':IDUsuario', $ID, PDO::PARAM_INT);
			    $qry->execute();
			    return $qry->fetchObject();
			} catch (Exception $ex) {
				return array('erro'=>'1', 'msg'=>'<strong>Erro!</strong> '.$ex->getMessage());
			};
		} else {
			return array('erro'=>'1', 'msg'=>'Não houve conexão com o Banco de Dados.');
		};

	}

	// Retorna informações do usuário se ele existir.
	// $ID: id do Usuário que deseja retornar as informações
	function UsuarioInfoIDHash($_IDHash) {

		$VIEW = "
			SELECT 
			    tbl_Usuarios . *,
			    concat_ws(' ',
			            tbl_Usuarios.NomePrimeiro,
			            tbl_Usuarios.NomeUltimo) AS NomeCompleto
			FROM
			    tbl_Usuarios			
		";
	
		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare($VIEW.' WHERE tbl_Usuarios._IDHash=:_IDHash LIMIT 1');
			    $qry->bindValue(':_IDHash', $_IDHash, PDO::PARAM_STR);
				$qry->execute();
			    if ($qry->rowCount() >= 1) {
					$Resposta['erro'] = '0';			// No início do array, adiciona a chave de erro 0
					$Resposta['msg'] = 'Usuário encontrado!';
			    	$Resposta[] = $qry->fetchObject();	// No index 0 coloca todas as informações vindas do banco de dados
					return $Resposta;
			    } else {
				    return array('erro'=>'1', 'msg'=>'Não foi localizado nenhum usuário com o código '.$_IDHash.'.');
			    };
			} catch (Exception $ex) {
				return array('erro'=>'1', 'msg'=>'<strong>Erro!</strong> '.$ex->getMessage());
			};
		} else {
			return array('erro'=>'1', 'msg'=>'Não houve conexão com o Banco de Dados.');
		};

	}
	
	// Zera a contagem de tentativas de login do usuário e grava data e hora do login no portal
	// $IDUsuario: ID do usuario que se deseja zerar
	function GravaAcessoUsuario($IDUsuario) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _TentativasLogin = 0, _UltimaTentativa = NULL, _UltimoLogin = now() WHERE IDUsuario=:IDUsuario');
			    $qry->bindValue(':IDUsuario', $IDUsuario, PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					return true;			    
			    } else {
				    return false;
			    };
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;
		};
		
	}

	// Grava senha de acesso do usuário no banco de dados
	// $Email: Email do usuario que se deseja gravar a senha
	// $SenhaAcesso: Senha sem criptografia do usuário
	function SalvaSenhaAcesso($Email, $SenhaAcesso, $NomeCompleto) {
	
		$SenhaAcessoHash = $this->CriaHash($SenhaAcesso);

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET SenhaAcesso=:SenhaAcesso, _CodigoAtivacao=NULL, _ResetSenha=NULL, _ResetData=NULL WHERE Email=:Email');
			    $qry->bindValue(':SenhaAcesso', $SenhaAcessoHash, 	PDO::PARAM_STR);
			    $qry->bindValue(':Email', 		$Email, 			PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
			    
				    $Dados[]=array("NomeCompleto", $NomeCompleto);
					$Dados[]=array("Email", $Email);
				    $Dados[]=array("Senha", $SenhaAcesso);
					$Dados[]=array("LinkPortal", URL_PORTAL);
				    $Template = 'senha.redefinida.html';
		
					$ConteudoHTML = $this->FormataEmail($Dados, $Template);
					if ($ConteudoHTML['erro'] == '0') {
		
						$ConteudoEmail = $ConteudoHTML['HTML'];
						$Assunto = 'Senha de Acesso';
					    $Categ = array('Conta', 'Nova Senha');
					    $Destinatarios[] = array('name'=>$NomeCompleto, 'email'=>$Email);
			
					    $EnviouEmail = $this->EnviaEmailMandrill($ConteudoEmail, $Assunto, $Categ, $Destinatarios);
						if ($ConteudoHTML['erro'] == '0') {
						
						    $UsuarioInfo = $this->UsuarioInfo($Email);					    
							return array('erro'=>'0', 'msg'=>'A senha foi salva com sucesso!', 'IDUsuario'=>$UsuarioInfo->IDUsuario);
						
						} else {
					    	return $EnviouEmail;				
						};
						
					} else {
				    	return $ConteudoHTML;				
					};
					
			    } else {
					return array('erro'=>'1', 'msg'=>'<strong>A senha não foi salva!</strong> Ocorreu algum erro ao tentar gravar a senha!');
			    };
			} catch (Exception $ex) {
				return array('erro'=>'1', 'msg'=>'<strong>A senha não foi salva!</strong> '.$ex->getMessage());
			};
		} else {
			return array('erro'=>'1', 'msg'=>'Não houve conexão com o Banco de Dados.');
		};
		
	}

	// Grava no banco a última tentativa de login do usuário
	// $IDUsuario: ID do usuário que tentou logar
	function GravaTentativaAcesso($IDUsuario) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _TentativasLogin = _TentativasLogin+1, _UltimaTentativa = :_UltimaTentativa WHERE IDUsuario=:IDUsuario');
				$qry->bindValue(':_UltimaTentativa', 	time(), 		PDO::PARAM_STR);
			    $qry->bindValue(':IDUsuario', 			$IDUsuario, 	PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					return true;			    
			    } else {
				    return false;
			    };
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;
		};
		
	}

	// Grava código de ativação da conta do usuário
	// $IDUsuario: ID do usuário para gravar código
	function GravaCodigoAtivacao($IDUsuario) {
	
		$_CodigoAtivacao = $this->CriaHash(uniqid(mt_rand(), true));

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _CodigoAtivacao = :_CodigoAtivacao WHERE IDUsuario=:IDUsuario');
				$qry->bindValue(':_CodigoAtivacao', $_CodigoAtivacao,	PDO::PARAM_STR);
			    $qry->bindValue(':IDUsuario', 		$IDUsuario, 		PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					return $_CodigoAtivacao;			    
			    } else {
				    return false;
			    };
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;
		};
		
	}

	// Cria um hash de verificação, salva esse hash no banco de dados depois envia email com link para o usuário
	// $EmailUsuario: email do Assessor que deseja enviar o email com o link para altera a senha
	function CriaHashParaResetSenha_EnviaEmailComLink($NomeCompleto, $EmailUsuario) {
	
        // generate timestamp (to see when exactly the user (or an attacker) requested the password reset mail)
        $DataResetSenha = time();
        // generate random hash for email password reset verification (40 char string)
        $HashResetSenha = sha1(uniqid(mt_rand(), true));

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _ResetSenha = :ResetSenha, _ResetData = :ResetData WHERE Email = :Email');
			    $qry->bindValue(':ResetSenha', 	$HashResetSenha, PDO::PARAM_STR);
			    $qry->bindValue(':ResetData', 	$DataResetSenha, PDO::PARAM_INT);
			    $qry->bindValue(':Email', 		$EmailUsuario, 	 PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
			    
				   	$link    = URL_PORTAL.'AlterarSenha.php?Email='.urlencode($EmailUsuario).'&CodVerif='.urlencode($HashResetSenha);

				    $Dados[]=array("NomeCompleto", $NomeCompleto);
					$Dados[]=array("LinkNovaSenha", $link);
				    $Template = 'redefinir.senha.html';
		
					$ConteudoHTML = $this->FormataEmail($Dados, $Template);
					if ($ConteudoHTML['erro'] == '0') {
		
						$ConteudoEmail = $ConteudoHTML['HTML'];
						$Assunto = 'Redefinir Senha de Acesso';
					    $Categ = array('Conta', 'Redefinição de Senha');
					    $Destinatarios[] = array('name'=>$NomeCompleto, 'email'=>$EmailUsuario);
			
					    $EnviouEmail = $this->EnviaEmailMandrill($ConteudoEmail, $Assunto, $Categ, $Destinatarios);
					    return $EnviouEmail;
						
					} else {
				    	return $ConteudoHTML;				
					};
				    
			    } else {
				    return array('erro'=>'1', 'msg'=>'Erro ao renovar a senha no banco de dados.');
			    };
			} catch (Exception $ex) {
				return array('erro'=>'1', 'msg'=>'Algum erro.');
			};
		} else {
			return array('erro'=>'1', 'msg'=>'Sem conexão com o banco de dados.');
		};
	}

	// Troca status do usuário para ativo no sistema
	// $IDUsuario: ID do usuario que se deseja alterar status
	function AtivarUsuario($IDUsuario) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _Ativo=1, _DataDesativacao=NULL, _CodigoAtivacao=NULL WHERE IDUsuario=:IDUsuario');
			    $qry->bindValue(':IDUsuario', $IDUsuario, PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Usuário ativado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "<strong>Erro!</strong> Status não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "<strong>Erro!</strong> ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca status do usuário para inativo no sistema
	// $IDUsuario: ID do usuario que se deseja alterar status
	function DesativarUsuario($IDUsuario) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _Ativo=0, _DataDesativacao=NOW() WHERE IDUsuario=:IDUsuario');
			    $qry->bindValue(':IDUsuario', $IDUsuario, PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Usuário desativado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "<strong>Erro!</strong> Status não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "<strong>Erro!</strong> ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca o primeiro nome do usuário
	// $IDUsuario: ID do usuario que se deseja alterar status
	// $NomePrimeiro: string do novo nome
	function AlterarNomePrimeiroUsuario($IDUsuario, $NomePrimeiro) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET NomePrimeiro=:NomePrimeiro WHERE IDUsuario=:IDUsuario LIMIT 1');
			    $qry->bindValue(':IDUsuario', 		$IDUsuario, 	PDO::PARAM_INT);
			    $qry->bindValue(':NomePrimeiro', 	$NomePrimeiro, 	PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Nome alterado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "<strong>Erro!</strong> Nome não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "<strong>Erro!</strong> ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca o último nome do usuário
	// $IDUsuario: ID do usuario que se deseja alterar status
	// $NomeUltimo: string do novo nome
	function AlterarNomeUltimoUsuario($IDUsuario, $NomeUltimo) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET NomeUltimo=:NomeUltimo WHERE IDUsuario=:IDUsuario LIMIT 1');
			    $qry->bindValue(':IDUsuario', 	$IDUsuario, 	PDO::PARAM_INT);
			    $qry->bindValue(':NomeUltimo', 	$NomeUltimo, 	PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Nome alterado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "Erro! Nome não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "Erro! ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca o avatar do usuário
	// $_IDHash: ID Hash do usuario que se deseja alterar status
	// $NomeImagem: string do nome da imagem
	function AlterarAvatarUsuario($_IDHash, $NomeImagem) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET Avatar=:Avatar WHERE _IDHash=:_IDHash LIMIT 1');
			    $qry->bindValue(':_IDHash', $_IDHash,	 PDO::PARAM_STR);
			    $qry->bindValue(':Avatar', 	$NomeImagem, PDO::PARAM_STR);
			    $qry->execute();

				$Resposta['erro'] = '0';
				$Resposta['msg'] = "Avatar alterado com sucesso!";
				return $Resposta;

			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "<strong>Erro!</strong> ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca o telefone comercial do usuário
	// $IDUsuario: ID do usuario que se deseja alterar status
	// $TelComercial: string do novo telefone
	function AlterarTelComercialUsuario($IDUsuario, $TelComercial) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET TelComercial=:TelComercial WHERE IDUsuario=:IDUsuario LIMIT 1');
			    $qry->bindValue(':IDUsuario', 		$IDUsuario, 	PDO::PARAM_INT);
			    $qry->bindValue(':TelComercial', 	$TelComercial, 	PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Telefone alterado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "Erro! Telefone não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "Erro! ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Troca o telefone celular do usuário
	// $IDUsuario: ID do usuario que se deseja alterar status
	// $TelCelular: string do novo telefone
	function AlterarTelCelularUsuario($IDUsuario, $TelCelular) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET TelCelular=:TelCelular WHERE IDUsuario=:IDUsuario LIMIT 1');
			    $qry->bindValue(':IDUsuario', 		$IDUsuario, 	PDO::PARAM_INT);
			    $qry->bindValue(':TelCelular', 		$TelCelular, 	PDO::PARAM_STR);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
					$Resposta['erro'] = '0';
					$Resposta['msg'] = "Telefone alterado com sucesso!";
					return $Resposta;
			    } else {
					$Resposta['erro'] = '1';
					$Resposta['msg'] = "Erro! Telefone não foi alterado.";
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "Erro! ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	// Retorna a listagem de todos os usuarios cadastrados
	// $NivelUsuario: Nivel do usuário que se deseja a lista: 0=Cliente; 1=Consultor; 2=Comerical; 3=Gestor
	//				  Pode ser um array separado por vírgula (ex.: 1,3)	
	// $Status: 0=Inativo; 1=Ativo
	//			Pode ser um array separado por vírgula (ex.: 0,1)	
	function AJAXListaUsuarios($NivelUsuario, $Status) {
		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare("SELECT * FROM tbl_Usuarios WHERE _NivelUsuario IN ($NivelUsuario) AND _Ativo IN ($Status) ORDER BY NomePrimeiro ASC");
				$qry->execute();
			    return $qry;//->fetchAll();
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;	
		};	
	}

	// Retorna a listagem de todos os usuarios cadastrados
	// $NivelUsuario: Nivel do usuário que se deseja a lista: 0=Cliente; 1=Consultor; 2=Comerical; 3=Gestor
	//				  Pode ser um array separado por vírgula (ex.: 1,3)	
	// $Status: 0=Inativo; 1=Ativo
	//			Pode ser um array separado por vírgula (ex.: 0,1)	
	function ListaUsuarios($NivelUsuario, $Status) {
	
		$VIEW = "
			SELECT 
			    tbl_Usuarios . *,
			    concat_ws(' ',
			            tbl_Usuarios.NomePrimeiro,
			            tbl_Usuarios.NomeUltimo) AS NomeCompleto,
				tbl_Clientes.NomeFantasia,
				tbl_Clientes.RazaoSocial
			FROM
			    tbl_Usuarios
			    	LEFT JOIN tbl_Clientes ON tbl_Usuarios._IDCliente = tbl_Clientes.IDCliente			
		";
	
		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare($VIEW." WHERE tbl_Usuarios._NivelUsuario IN ($NivelUsuario) AND tbl_Usuarios._Ativo IN ($Status) ORDER BY tbl_Usuarios.NomePrimeiro ASC");
				$qry->execute();
			    return $qry->fetchAll();
			} catch (Exception $ex) {
				return false;
			};
		} else {
			return false;	
		};	
	}

	// Cadastra um novo usuário no bando de dados
	function InserirUsuario($Avatar, $NomePrimeiro, $NomeUltimo, $Email, $SenhaAcesso, $TelComercial, $TelCelular, $_NivelUsuario, $_IDCliente) {

		$SenhaSemHash = $SenhaAcesso;
		$SenhaAcesso = (($SenhaAcesso == "")?NULL:$this->CriaHash($SenhaAcesso));

		if ($this->ConexaoMySQL()) {
			try {
				$qry = $this->conexao->prepare('INSERT INTO tbl_Usuarios (Avatar, NomePrimeiro, NomeUltimo, Email, SenhaAcesso, TelComercial, TelCelular, _NivelUsuario, _IDCliente, _DataRegistro) VALUES (:Avatar, :NomePrimeiro, :NomeUltimo, :Email, :SenhaAcesso, :TelComercial, :TelCelular, :_NivelUsuario, :_IDCliente, NOW())');
			    $qry->bindValue(':Avatar', 			$Avatar,		PDO::PARAM_STR);
			    $qry->bindValue(':NomePrimeiro',	$NomePrimeiro, 	PDO::PARAM_STR);
			    $qry->bindValue(':NomeUltimo', 		$NomeUltimo,	PDO::PARAM_STR);
			    $qry->bindValue(':Email', 			$Email, 		PDO::PARAM_STR);
			    $qry->bindValue(':SenhaAcesso', 	$SenhaAcesso, 	PDO::PARAM_STR);
			    $qry->bindValue(':TelComercial',	$TelComercial, 	PDO::PARAM_STR);
			    $qry->bindValue(':TelCelular', 		$TelCelular, 	PDO::PARAM_STR);
			    $qry->bindValue(':_NivelUsuario', 	$_NivelUsuario, PDO::PARAM_INT);
			    $qry->bindValue(':_IDCliente', 		$_IDCliente, 	PDO::PARAM_INT);
			    $qry->execute();
			    
			    if ($qry->rowCount() == 1) {
			    // inseriu sem problemas
			    
			    	$ID = $this->conexao->lastInsertId();
			    
					// Preeche a coluna _IDHash da tabela com o ID que acabou de receber					
			    	$GravouHash = $this->GravaHash('tbl_Usuarios', 'IDUsuario', $ID, $HashGravado);
			    	
			    	// Gera e grava um hash randômico como um código de verificação para ativação da conta do usuário
			    	$_CodigoAtivacao = $this->GravaCodigoAtivacao($ID);
			    
					if ($GravouHash && $_CodigoAtivacao) {

						// Retorna as informações gravadas
				        $Resposta = array(
				        	'erro'=>'0', 'msg'=>'Usuário cadastrado com sucesso!',
				        	'IDUsuario'=>$ID,
				        	'NomeCompleto'=>$NomePrimeiro.' '.$NomeUltimo,
				        	'Email'=>$Email,
				        	'SenhaAcesso'=>$SenhaSemHash,
				        	'_CodigoAtivacao'=>$_CodigoAtivacao
				        );
				        return $Resposta;										
					    						
					} else {
						$Resposta['erro'] = '1';
						$Resposta['msg'] = 'Não foi possível gravar o código do Usuário.';
						return $Resposta;						
					};
			    
			    } else {
			    	$ErroSQL = $qry->errorInfo();			    
					$Resposta['erro'] = '1';
					$Resposta['msg'] = 'Nenhum registro foi inserido. Erro: '.$ErroSQL[1].': '.$ErroSQL[2];
					return $Resposta;			
			    };
	
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = 'Nenhum registro foi inserido. Erro: '.$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = 'Não houve conexão com o Banco de Dados.';
			return $Resposta;			
		};

	}


	/*
	*	MÓDULO DE 'USUÁRIOS'
   *    -------------------------------------------------------- */	


	/*	--------------------------------------------------------
	*	FUNÇÕES CONTROLE DO SERIAL
   */	

	// Grava o serial do usuário na tabela
	// $IDUsuario: ID do usuario que se deseja inserir o serial
	function GravaSerial($IDUsuario, $Serial) {

		if ($this->ConexaoMySQL()) {
			try {
			    $qry = $this->conexao->prepare('UPDATE tbl_Usuarios SET _Ativo=1, Serial=:Serial WHERE IDUsuario=:IDUsuario');
			    $qry->bindValue(':Serial', 		$Serial, 	PDO::PARAM_STR);
			    $qry->bindValue(':IDUsuario', 	$IDUsuario, PDO::PARAM_INT);
			    $qry->execute();
			    if ($qry->rowCount() == 1) {
		    
					// Retorna as informações gravadas
			        $Resposta = array(
			        	'erro'=>'0', 'msg'=>'Serial gravado com sucesso!',
			        	'IDUsuario'=>$IDUsuario,
			        	'Serial'=>$Serial
			        );
			        return $Resposta;

			    } else {
			    	$ErroSQL = $qry->errorInfo();			    
					$Resposta['erro'] = '1';
					$Resposta['msg'] = 'Nenhum registro foi inserido. Erro: '.$ErroSQL[1].': '.$ErroSQL[2];
					return $Resposta;
			    };
			} catch (Exception $ex) {
				$Resposta['erro'] = '1';
				$Resposta['msg'] = "<strong>Erro!</strong> ".$ex->getMessage();
				return $Resposta;
			};
		} else {
			$Resposta['erro'] = '1';
			$Resposta['msg'] = "<strong>Erro!</strong> Não houve conexão com o Banco de Dados.";
			return $Resposta;			
		};
		
	}

	/*
	*	FUNÇÕES CONTROLE DO SERIAL
   *    -------------------------------------------------------- */	








	/*	--------------------------------------------------------
	*	FUNÇÕES DE TRATAMENTO DE DADOS
   */	

	// Função que cria o hash de uma string antes de gravar no banco de dados
	// A definição da chave de criptografia HASH_SENHA está em config.php
	// $Str: String a ser criptografada
	function CriaHash($Str) {
		return sha1($Str.HASH_SENHA);
	}
	
	// Formata o tamanho de um arquivo
	// $bytes: tamanho do arquivo em bytes
	// $precision: casas decimais
	function formataBytes($bytes, $precision = 2) { 
	    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
	
	    $bytes = max($bytes, 0); 
	    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	    $pow = min($pow, count($units) - 1); 
	
	    // Uncomment one of the following alternatives
	    // $bytes /= pow(1024, $pow);
	    $bytes /= (1 << (10 * $pow)); 
	
	    return round($bytes, $precision) . ' ' . $units[$pow]; 
	} 

	// Formata a data (string) para o formato do MySQL
	// $data: Data no formtado de string (Ex.: 12/04/2014)
	function formataDataMysql($data) {				
		$datavet = explode("-", str_replace("/","-",$data));
		$tam = count($datavet);
			
		$dia = "";
		$mes = "";
		$ano = "";
			
		if($tam > 0)
		  $dia = $datavet[0];
			
		if($tam > 1)  
		  $mes = $datavet[1]."-";
			  
		if($tam > 2 )
		  $ano = $datavet[2]."-";
			
		$ndata = $ano.$mes.$dia;
	
		return $ndata;
	}

	// Formata a data e hora (string) para o formato do MySQL
	// $datahora: Data no formtado de string (ex.: 2014-04-12 20:22:51)
	function formataDataHoraMysql($datahora) {
		
		$DuasPartes = explode(" ", $datahora);
		
		$datavet = explode("-", str_replace("/","-",$DuasPartes[0]));
		$tam = count($datavet);
			
		$dia = "";
		$mes = "";
		$ano = "";
			
		if($tam > 0)
		  $dia = $datavet[0];
			
		if($tam > 1)  
		  $mes = $datavet[1]."-";
			  
		if($tam > 2 )
		  $ano = $datavet[2]."-";
			
		$ndata = $ano.$mes.$dia;
	
		return $ndata.' '.$DuasPartes[1];
	}

	// Formata somente a data ou data e hora (string) para o formato do MySQL
	// $datahora: Data no formtado de string (ex.: 2014-04-12 20:22:51 ou 2014-04-12)
	function formataDtaHraMysql($datahora) {
		
		$DuasPartes = explode(" ", $datahora);
		
		if (count($DuasPartes) > 1) {

			$datavet = explode("-", str_replace("/","-",$DuasPartes[0]));
			$tam = count($datavet);
				
			$dia = "";
			$mes = "";
			$ano = "";
				
			if($tam > 0)
			  $dia = $datavet[0];
				
			if($tam > 1)  
			  $mes = $datavet[1]."-";
				  
			if($tam > 2 )
			  $ano = $datavet[2]."-";
				
			$ndata = $ano.$mes.$dia;
		
			return $ndata.' '.$DuasPartes[1];
			
		} else {

			$datavet = explode("-", str_replace("/","-",$datahora));
			$tam = count($datavet);
				
			$dia = "";
			$mes = "";
			$ano = "";
				
			if($tam > 0)
			  $dia = $datavet[0];
				
			if($tam > 1)  
			  $mes = $datavet[1]."-";
				  
			if($tam > 2 )
			  $ano = $datavet[2]."-";
				
			$ndata = $ano.$mes.$dia;
		
			return $ndata;

		};
		
	}

	// Formata a data do MySQL para o formato dd/mm/aaaa
	// $data: Data no formtado de string (ex.: 2014-04-12)
	function formataData($data) {
		if ($data == "") {
			return "";
		} else {
			return date("d/m/Y", strtotime($data));	
		};
	}
	
	// Formata a data do MySQL para o formato dd/mm/aaaa h:m:s
	// $datahora: Data no formtado de string (ex.: 2014-04-12 20:22:51)
	function formataDataHora($datahora) {
		if ($datahora == "") {
			return "";
		} else {
			return date("d/m/Y H:i", strtotime($datahora));	
		};
	}

	// Formata a data do MySQL para o formato dd/mmm
	// $data: Data no formtado de string (ex.: 2014-04-12)
	function formataDataResumida($data) {
		if ($data == "") {
			return "";
		} else {
		
			$Dia = date("d", strtotime($data));
			$Mes = date("m", strtotime($data));
			
			$Mes = substr($this->Mes($Mes), 0, 3);
		
			return $Dia.'/'.$Mes;	
		};
	}

	// Formata a data do MySQL para o formato somente horas hh:mm
	// $data: Data no formtado de string (ex.: 2014-04-12 08:00:00)
	function formataHora($data) {
		if ($data == "") {
			return "";
		} else {
		
			$Hora = date("H:i", strtotime($data));
					
			return $Hora;	
		};
	}

	// Retorna o nome do mês
	function Mes($NumMes) {
		switch($NumMes) {
			case 1:  return "Janeiro";
			case 2:  return "Fevereiro";
			case 3:  return "Março";
			case 4:  return "Abril";
			case 5:  return "Maio";
			case 6:  return "Junho";
			case 7:  return "Julho";
			case 8:  return "Agosto";
			case 9:  return "Setembro";
			case 10: return "Outubro";
			case 11: return "Novembro";
			case 12: return "Dezembro";
		};	
	}

	// Calcula a diferênça entre duas datas
	// $DtaIni e $DtaFim: datas no formato PHP (aaaa-mm-dd hh:mm:ss)
	function DifDatas ($DtaIni, $DtaFim, &$TextoMedida) {		
		
		// Calcula a diferença em segundos entre as datas
		$dif = strtotime($DtaFim) - strtotime($DtaIni);
		
		// Se as datas estiverem invertidos, troca a subtração
		if ($dif < 0) {
			$dif =  strtotime($DtaIni) - strtotime($DtaFim);
		};
		
		//Calcula a diferença em dias
		//$tempo = floor($dif / (60 * 60 * 24));

		// Recupera os anos
		$anos = floor($dif/ (60 * 60 * 24 * 30 * 12));		
		// Recupera os anos
		$meses = floor($dif/ (60 * 60 * 24 * 30));
		// Recupera os dias
		$dias  = floor($dif/86400);
		// Recupera as horas
		$horas = floor(($dif-($dias*86400))/3600);
		// Recupera os minutos
		$mins  = floor(($dif-($dias*86400)-($horas*3600))/60);
		// Recupera os segundos
		$secs  = floor($dif-($dias*86400)-($horas*3600)-($mins*60));


		if ($anos > 0) {
			$TextoMedida = (($anos==1)?'ano':'anos');
			return $anos;
			
		} elseif ($meses > 0) {
			$TextoMedida = (($meses==1)?'mês':'meses');
			return $meses;

		} elseif ($dias > 0) {
			$TextoMedida = (($dias==1)?'dia':'dias');
			return $dias;		
			
		} elseif ($horas > 0) {
			$TextoMedida = (($horas==1)?'hora':'horas');
			return $horas;		
				
		} elseif ($mins > 0) {
			$TextoMedida = (($mins==1)?'minuto':'minutos');
			return $mins;			
		
		} elseif ($secs > 0) {
			$TextoMedida = (($secs==1)?'segundo':'segundos');
			return $secs;			
		
		} else {
			$TextoMedida = 'erro';
			return 0;						
		}
		
		
	}


	/*
	*	FUNÇÕES DE TRATAMENTO DE DADOS
   *    -------------------------------------------------------- */	



	/*	--------------------------------------------------------
	*	ENVIO DE EMAILS
   */	

   // Substitui os dados dentro de um template e retorna o HTML resultante
   function FormataEmail($Dados, $Modelo) {
		
		// Define o nome completo do arquivo de modelo do email
		$Arquivo = CAMINHO_MODELOS_EMAIL.$Modelo;
		
		// Abre e lê o arquivo de modelo
		// 'r' = abrir somente leitura
		// o '@' é para não mostrar mensagem de erro evitando o erro 200 no AJAX
		$fd = @fopen ($Arquivo, "r");
		
		// Confere se o arquvo existe
		if ($fd) {
				
			$ConteudoEmail = fread ($fd, filesize ($Arquivo));
			// Fecha o arquivo aberto		
			fclose ($fd);
			
			// Percorre o Array $Dados preenchendo o modelo de email com as informações
			foreach ($Dados as $key=>$valor){
				$ConteudoEmail = str_replace("%%$valor[0]%%", $valor[1], $ConteudoEmail);
			};	
		
			$Resposta = array('erro'=>'0', 'msg'=>'Email formatado com sucesso!', 'HTML'=>$ConteudoEmail);		    			    			    
		
		} else {
			$Resposta = array('erro'=>'1', 'msg'=>'Arquivo de modelo ('.$Modelo.') não encontrado.');		    			    			    
		};
		
		return $Resposta;
   
   }

   // Envia email utilizando a classe do Mandrill (https://mandrillapp.com)
   function EnviaEmailMandrill($ConteudoEmail, $Assunto, $Categ, $Destinatarios) {
	   
	   // Adiciona a classe do Mandrill
	   require_once 'bib/mandrill-api/Mandrill.php';
	   		
		try {
			
			$mandrill = new Mandrill(API_KEY);
		    $message = array(
		    	'html' => $ConteudoEmail,
		        'subject' => '['.NOME_SISTEMA.'] '.$Assunto,
		        'from_email' => EMAIL_FROM,
		        'from_name' => EMAIL_FROM_NAME,
		        'to' => $Destinatarios,
		        //'headers' => array('Reply-To' => 'emailresposta@officeapp.com.br'),
		        //'bcc_address' => 'helbertcampos@outlook.com',
		        'tags' => $Categ,
		        'signing_domain' => 'interativaconsultoria.com.br',
		        'subaccount' => SUBACCOUNT,
		    );
		    $async = false;
		    
		    $result = $mandrill->messages->send($message, $async);
			
		    if ($result[0]['status'] == 'sent') {
				$Resposta = array('erro'=>'0', 'msg'=>'<strong>Email enviado com sucesso!</strong>');		    			    			    
		    } else {
				$Resposta = array('erro'=>'1', 'msg'=>'<strong>Erro ao enviar email!</strong> ' . json_encode($result));		    			    
		    };
		    
		} catch(Mandrill_Error $e) {
			$Resposta = array('erro'=>'1', 'msg'=>'<strong>Erro ao enviar email!</strong> ' . get_class($e) . ' - ' . $e->getMessage());		    
		}
		
		return $Resposta;
   
   }

	/*
	*	ENVIO DE EMAILS
   *    -------------------------------------------------------- */	



}

?>